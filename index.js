function tienLuong() {
    var one_day_salary = document.getElementById("txt-luong-1-ngay").value;
    var work_number_date = document.getElementById("txt-so-ngay-lam").value;
    var result = 0;
    result = one_day_salary * work_number_date;
    document.getElementById("tongLuong").innerHTML = result;
}
function Average() {
    var num1 = document.getElementById("txt-num1").value;
    var num2 = document.getElementById("txt-num2").value;
    var num3 = document.getElementById("txt-num3").value;
    var num4 = document.getElementById("txt-num4").value;
    var num5 = document.getElementById("txt-num5").value;
    var result = 0;
    result = ((num1 * 1 + num2 * 1 + num3 * 1 + num4 * 1 + num5 * 1) / 5);
    document.getElementById("trungBinh").innerHTML = result;
}
function Money() {
    var usd = document.getElementById("txt-money").value;
    var result = 0;
    result = usd * 23500;
    document.getElementById("Tien").innerHTML = new Intl.NumberFormat('vn-VN').format(result);
}
function Rectangle() {
    var long = document.getElementById("txt-long").value;
    var width = document.getElementById("txt-width").value;
    var area = 0;
    area = long * width;
    var perimeter = 0;
    perimeter = (long * 1 + width * 1) * 2;
    document.getElementById("Perimeter").innerHTML = "Chu vi:" + perimeter;
    document.getElementById("Area").innerHTML = "Diện tích:" + area + ";";
}
function Sum() {
    var num = document.getElementById("txt-num").value;
    var tens = Math.floor(num / 10);
    var unit = num % 10;
    var result = 0;
    result = unit + tens;
    document.getElementById("sum").innerHTML = result;
}